from django.contrib import admin
from .models import ProductMaster, Products, Purchase, PurchasePayment
from .forms import ProductsForm


class ProductMasterAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'product_type',
    ]


class ProductsAdmin(admin.ModelAdmin):

    # def quantity(self, data):
    #     quantity = Inventory.objects.filter(product=data).count()
    #     return quantity

    form = ProductsForm
    list_display = [
        'product_id',
        'product_name',
        'ptype',
        'purchase_price_avg',
        'sell_price',
        'quantity',
        'tax']
    search_fields = [
        'id',
        'product_name']
    list_filter = ['ptype']


class PurchasePaymentAdmin(admin.StackedInline):
    model = PurchasePayment
    extra = 1

class PurchaseAdmin(admin.ModelAdmin):
    inlines = [PurchasePaymentAdmin]

    list_display = [
        'id',
        'product',
        'quantity',
        'purchase_price',
        'created_on']
    search_fields = [
        'id',
        'product__product_name']
    list_filter = ['product', ]


admin.site.register(ProductMaster, ProductMasterAdmin)
admin.site.register(Products, ProductsAdmin)
admin.site.register(Purchase, PurchaseAdmin)
