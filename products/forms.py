from django import forms
from .models import Products

class ProductsForm(forms.ModelForm):
    class Meta:
        model = Products
        fields = [
            'product_id',
            'product_name',
            'ptype',
            'hsn_code',
            'sell_price',
            'tax',
            'description']