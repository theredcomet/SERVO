from django.test import TestCase

from sales.funcs import get_test_data

from .models import ProductMaster, Products


class ProductMasterTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()

    def test_create_master(self):
        master = ProductMaster(product_type=self.data.get("ptype"))
        master.save()
        self.assertEqual(master.product_type, self.data.get("ptype"))


class ProductTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()

        self.master = ProductMaster(product_type=self.data.get("ptype"))
        self.master.save()

    def test_create_product(self):
        ptype = ProductMaster.objects.get(product_type=self.data.get("ptype"))
        product = Products(
            product_name=self.data.get("pname"),
            ptype=ptype,
            sell_price=self.data.get("sell_price"),
            tax=self.data.get("tax"))
        product.clean()
        product.save()
        self.assertAlmostEqual(self.data.get("pname"), product.product_name)

class ProductAveragePriceTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()

        self.master = ProductMaster(product_type=self.data.get("ptype"))
        self.master.save()

    def test_create_productprice(self):
        ptype = ProductMaster.objects.get(product_type=self.data.get("ptype"))
        product = Products(
            product_name=self.data.get("pname"),
            ptype=ptype,
            sell_price=self.data.get("sell_price"),
            tax=self.data.get("tax"))
        product.clean()
        product.save()
        self.assertAlmostEqual(self.data.get("sell_price"), product.purchase_price_avg)
