# from rest_framework import routers, viewsets
# from rest_framework import routers, serializers, viewsets
# # from rest_framework_json_api import serializers, pagination
# from rest_framework.response import Response
# from rest_framework.pagination import LimitOffsetPagination

# from .models import Purchase, ProductMaster, Products
# from url_filter.integrations.drf import DjangoFilterBackend

# from SERVO.pagination import Pagination

# # Serializers define the API representation.


# class ProductMasterSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = ProductMaster
#         fields = ('id', 'product_type')


# class ProductMasterViewSet(viewsets.ModelViewSet):
#     queryset = ProductMaster.objects.all()
#     serializer_class = ProductMasterSerializer
#     pagination_class = Pagination

#     # def list(self, request, *args, **kwargs):
#     #     queryset = ProductMaster.objects.all()
#     #     serializer = ProductMasterSerializer(queryset, many=True)
#     #     headers = {'x-total-count': queryset.count(),
#     #         'access-control-allow-origin': '*'}
#     #     response = Response(serializer.data, headers=headers)
#     #     return response

# class ProductMasterViewSetRaw(viewsets.ModelViewSet):
#     queryset = ProductMaster.objects.all()
#     serializer_class = ProductMasterSerializer


# class ProductsSerializer(serializers.ModelSerializer):
#     ptype = ProductMasterSerializer()

#     class Meta:
#         model = Products
#         fields = ('id', 'product_name', 'ptype', 'purchase_price_avg',
#                   'sell_price', 'description', 'quantity', 'tax')


# class ProductsViewSet(viewsets.ModelViewSet):
#     queryset = Products.objects.all()
#     serializer_class = ProductsSerializer
#     filter_fields = ['id', 'product_name']
#     pagination_class = Pagination


# class InventorySerializer(serializers.ModelSerializer):
#     product = ProductsSerializer()

#     class Meta:
#         model = Purchase
#         fields = ('id', 'product', 'quantity', 'purchase_price',
#                   'quantity', 'quality_rating', 'created_on')


# class InventoryViewSet(viewsets.ModelViewSet):
#     queryset = Purchase.objects.all()
#     serializer_class = InventorySerializer
#     filter_fields = ['id', 'product']
#     pagination_class = Pagination

