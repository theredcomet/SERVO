import uuid

from django.db import models

from contacts.models import Supplier


class ProductMaster(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    product_type = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.product_type


class Products(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    product_id = models.IntegerField(unique=True, null=True)

    product_name = models.CharField(max_length=100, unique=True)
    ptype = models.ForeignKey(
        ProductMaster, on_delete=models.CASCADE, verbose_name="Product Type")
    hsn_code = models.CharField(max_length=100, null=True)
    purchase_price_avg = models.PositiveIntegerField(
        help_text="Average of purchase price")
    sell_price = models.PositiveIntegerField()
    description = models.CharField(max_length=250, null=True, blank=True)
    quantity = models.PositiveIntegerField(default=0, blank=True)
    tax = models.FloatField()

    def clean(self):
        if not self.purchase_price_avg:
            self.purchase_price_avg = self.sell_price

    def __str__(self):
        return self.product_name


class Purchase(models.Model):
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    batch_id = models.CharField(max_length=100, null=True)
    quantity = models.PositiveIntegerField(default=1)
    purchase_price = models.PositiveIntegerField()
    quality_rating = models.CharField(max_length=50, null=True, blank=True)
    supplier = models.ForeignKey(Supplier, on_delete=models.CASCADE)                                                                                                                     
    created_on = models.DateTimeField(auto_now_add=True)

    manufacturing_date = models.DateField(null=True)
    expiry_date = models.DateField(null=True)

    def clean(self):
        self.product.quantity = self.quantity + self.product.quantity
        self.product.save()

    def __str__(self):
        return '{}'.format(self.id)

class PurchasePayment(models.Model):
    purchase = models.ForeignKey(Purchase, on_delete=models.CASCADE)
    amount = models.FloatField(default=0.0)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}'.format(self.id)