# SERVO

Django powered web application to Manage Auto Mobile Showroom and Service station.

## Modules

1.  Accounts
2.  Activities
3.  Users

### 1. ACCOUNTS

Account section maintains cash flow, reciept generation and customer creation.

### 2. Activities

Activities are tracked in this modules

-   Sales
-   Services
-   Inventory

### 3. USERS

This module is built on (not extended at all) on django-admin as defined in *Roles section*.

## ROLES

This project has two level of users

1.  Admin
2.  Account manager

__ACCOUNT MANAGER__

1.  Create new Invoices
2.  Generate cash receipts
3.  Inventory Entry
4.  Create new customer
5.  Create service entries
6.  Create sales entries

__Admin roles__

1.  Create Account Manager
2.  Manage Invoices
3.  Inventory Management
4.  Customer Management
5.  Sales Management
6.  Service Management

*Management here refers to creation, updation, deletion of entries*

