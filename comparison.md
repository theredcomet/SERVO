# ZOHO INVENTORY
## _https://inventory.zoho.com_

## On Signup

*   TimeZone (default is San Francisco)
*   Organisation Name
*   Portal Name: appends with https://inventory.zoho.com/portal/{}
*   Business Location
*   Industry 
    *   Agency or sales house
    *   Agriculture
    *   Art & Design
    *   Automotive
    *   Construction
    *   Consulting
    *   Consumer Packaged Goods
    *   Education
    *   Engineering
    *   Entertainment
    *   Financial Services
    *   Food Services
    *   Food Services (Restaurants Fast Food)
    *   Gaming
    *   Government
    *   Health Care
    *   Interior Design
    *   Legal
    *   Manufacturing
    *   Marking
    *   Mining & Logistics
    *   Non-Profit
    *   Publishing and Web Media
    *   Real Estate
    *   Retail (E-commerce & offline)
    *   Services
    *   Technology
    *   Telecommunications
    *   Travel/Hospitality
    *   Web Designing
    *   Web Development
    *   Writers
*   Company Address 
*   Regional Settings
    *   Currency
    *   Language
    *   Time Zone
    *   Inventory Start Date
    *   Fiscal Year

## Getting Started Guide

* Trial : 14 days

