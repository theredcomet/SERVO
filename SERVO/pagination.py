from collections import OrderedDict
from rest_framework.response import Response

from rest_framework.pagination import PageNumberPagination


class Pagination(PageNumberPagination):

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            # ('count', self.page.paginator.count),
            # ('next', self.get_next_link()),
            # ('previous', self.get_previous_link()),
            ('list', data)
        ]))
