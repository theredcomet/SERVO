from django.test import TestCase
from django.conf import settings

from products.models import Products, ProductMaster
from contacts.models import Customer
from .models import SalesRecord, PaymentRecord
from sales.funcs import get_test_data


class SalesRecordTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()

        self.customer = Customer(
            customer_name=self.data.get("customer_name"),
            phone_number=self.data.get("phone_number"))
        self.customer.save()

        master = ProductMaster(product_type=self.data.get("ptype"))
        master.save()

        ptype = ProductMaster.objects.get(product_type=self.data.get("ptype"))

        product = Products(
            product_name=self.data.get("pname"),
            ptype=ptype,
            sell_price=self.data.get("sell_price"),
            tax=self.data.get("tax"))
        product.clean()
        product.save()

        self.product = Products.objects.get(
            product_name=self.data.get("pname"))

    def test_create_sale(self):
        new_sales = SalesRecord(
            product=self.product,
            sale_price=self.data.get("sell_price"),
            customer=self.customer)
        new_sales.save()
        self.assertEqual(new_sales.sale_price, self.data.get("sell_price"))


class PaymentRecordTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()

        self.customer = Customer(
            customer_name=self.data.get("customer_name"),
            phone_number=self.data.get("phone_number"))
        self.customer.save()

        master = ProductMaster(product_type=self.data.get("ptype"))
        master.save()

        ptype = ProductMaster.objects.get(product_type=self.data.get("ptype"))

        product = Products(
            product_name=self.data.get("pname"),
            ptype=ptype,
            sell_price=self.data.get("sell_price"),
            tax=self.data.get("tax"))
        product.clean()
        product.save()

        self.product = Products.objects.get(
            product_name=self.data.get("pname"))

        self.sales = SalesRecord(
            product=self.product,
            sale_price=self.data.get("sell_price"),
            customer=self.customer)
        self.sales.save()

    def test_create_payment(self):
        new_payment = PaymentRecord(
            amount=self.data.get("paid_amount"),
            sale=self.sales)
        new_payment.clean()
        new_payment.save()
        self.assertEqual(
            new_payment.cash_receipt.name,
            "../media/cashreceipts/{}.pdf".format(
            new_payment.cash_receipt_id))
        self.assertEqual(new_payment.cash_receipt_id, 1)
