from django.conf import settings
from django.db.models.signals import post_save
from django.db.models import Sum
from django.dispatch import receiver
from django.template.loader import render_to_string
from weasyprint import CSS, HTML
from weasyprint.fonts import FontConfiguration

from .funcs import ufill
from .models import PaymentRecord, SalesRecord, ProductMatrix


@receiver(post_save, sender=PaymentRecord)
def create_cashreceipt(sender, **kwargs):
    if not kwargs.get("raw"):
        instance = kwargs.get("instance")

        filename = 'media/cashreceipts/{}.pdf'.format(
            instance.cash_receipt_id)
        cash_id = str(instance.cash_receipt_id).zfill(6)
        customer = instance.sale.customer
        amount = ufill(str(instance.amount), 24)
        products = ProductMatrix.objects.filter(sale=instance.sale)

        jdata = {
            "company_name": "SOUTHERN FORMULATIONS",
            "address": "Tatvamasi, 2nd Stage, ORR, Bangalore-58",
            "email_id": "pragitham@rediffmail.com",
            "phone_number": "9686205054",
            "cash_id": cash_id,
            "payment_date": instance.paid_date,
            "customer": customer,
            "amount": amount,
            "products": products,
            "payment_type": "Cash"}
        # stylesheets = [
        #     CSS("{}/sales/semantic.min.css".format(settings.STATIC_ROOT))]
        html_content = render_to_string("sales/cash_receipt.html", jdata)
        pdf_file = HTML(string=html_content).write_pdf(
            stylesheets=[])
        with open(filename, "wb") as pdffile:
            pdffile.write(pdf_file)

        instance.cash_receipt = '../{}'.format(filename)
        instance.save_base(raw=True)

        # all_pays = PaymentRecord.objects.filter(sale)
        # all_pays = Pay
        if(instance.sale.sale_price) == PaymentRecord.objects.aggregate(Sum('amount')).get('amount__sum'):
            instance.sale.status == 1
