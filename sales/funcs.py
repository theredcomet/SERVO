import json

def get_test_data():
    filename = "test_data.json"
    with open(filename, "rt") as jsonfile:
        return json.loads(jsonfile.read())

def ufill(text, length):
    rvalue = text
    while len(rvalue) < length:
        rvalue += '_'
    return rvalue