
import time
import uuid

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Sum
from django.template.loader import render_to_string
from weasyprint import CSS, HTML

from contacts.models import Customer, Transport, Representative
from products.models import Purchase, ProductMaster, Products

from .funcs import ufill


def get_cashreciept_dir(instance, filename):
    return '{}'.format(filename)


def amount_validator(data):
    pvalue = PaymentRecord.objects.filter(sale=data.sale) \
        .only('amount') \
        .aggregate(Sum('amount'))
    pvalue = pvalue.get("amount__sum", 0)
    if not pvalue:
        pvalue = 0
    cvalue = data.amount
    total = pvalue + cvalue
    if total > data.sale.sale_price:
        return False
    return True


class SalesRecord(models.Model):

    DELIVERY_TYPE = (
        (0, "Door Delivery"),
        (1, "CC"),
        (2, "2 pay"),
    )

    PAYMENT_STATUS = (
        (0, "Unpaid"),
        (1, "Paid"),
    )

    id = models.AutoField(primary_key=True)
    # product = models.ForeignKey(Products, on_delete=models.CASCADE)
    sale_price = models.PositiveIntegerField(default=0)
    quantity = models.PositiveIntegerField(default=1)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    representative = models.ForeignKey(
        Representative, on_delete=models.CASCADE, null=True)
    sale_date = models.DateTimeField(auto_now_add=True, editable=True)
    dispatch = models.ForeignKey(
        Transport, on_delete=models.CASCADE, null=True)
    type_of_delivery = models.IntegerField(choices=DELIVERY_TYPE, default=0)

    p_status = models.IntegerField(choices=PAYMENT_STATUS, default=0, verbose_name="Payment Status") 

    @property
    def hsn_code(self):
        return self.product.hsn

    def __str__(self):
        return '{}'.format(self.id)


class ProductMatrix(models.Model):
    sale = models.ForeignKey(SalesRecord, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    sale_price = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.product.product_name

class PaymentRecord(models.Model):
    cash_receipt_id = models.AutoField(primary_key=True)
    amount = models.PositiveIntegerField()
    paid_date = models.DateTimeField(auto_now_add=True)
    sale = models.ForeignKey(SalesRecord, on_delete=models.CASCADE)
    cash_receipt = models.FileField(
        upload_to=get_cashreciept_dir,
        null=True, blank=True)

    def clean(self):
        if not amount_validator(self):
            raise ValidationError("Paid amount is exceeding Sale price")

    def __str__(self):
        return '{}'.format(self.cash_receipt_id)
