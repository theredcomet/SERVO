from django.contrib import admin
from django.db.models import Sum
from .models import PaymentRecord, SalesRecord, ProductMatrix

# from rangefilter.filter import DateRangeFilter

class PaymentRecordInline(admin.TabularInline):
    model = PaymentRecord

    readonly_fields = ['cash_receipt',]
    extra = 1


class ProductMatrix(admin.TabularInline):
    model = ProductMatrix
    extra = 1

class SalesRecordAdmin(admin.ModelAdmin):
    def paid_amount(self, data):
        rvalue = PaymentRecord.objects.filter(sale=data) \
            .only('amount') \
            .aggregate(Sum('amount'))
        return rvalue.get('amount__sum')

    inlines = [ProductMatrix, PaymentRecordInline]
    list_filter = ['representative', 'p_status']

    list_display = [
        # 'id',
        # 'product',
        'sale_price',
        'paid_amount',
        'customer',
        'representative',
        'p_status',
        'sale_date']


admin.site.register(SalesRecord, SalesRecordAdmin)
admin.site.site_header = "Southern Veterinary Group"
admin.site.site_title = "Southern Veterinary group"