import time
import csv
import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from geodata.models import Country, State, City


class Command(BaseCommand):
    help = 'Imports csv data to models'

    def handle(self, *args, **options):
        Country.objects.all().delete()
        State.objects.all().delete()
        City.objects.all().delete()
        
        country = Country.objects.create(country_name='India')
        country = Country.objects.get(country_name='India')

        states = [
            'ANDAMAN AND NICOBAR ISLANDS',
            'ANDHRA PRADESH',
            'ARUNACHAL PRADESH',
            'ASSAM',
            'BIHAR',
            'CHATTISGARH',
            'CHANDIGARH',
            'DAMAN AND DIU',
            'DELHI',
            'DADRA AND NAGAR HAVELI',
            'GOA',
            'GUJARAT',
            'HIMACHAL PRADESH',
            'HARYANA',
            'JAMMU AND KASHMIR',
            'JHARKHAND',
            'KERALA',
            'KARNATAKA',
            'LAKSHADWEEP',
            'MEGHALAYA',
            'MAHARASHTRA',
            'MANIPUR',
            'MADHYA PRADESH',
            'MIZORAM',
            'NAGALAND',
            'ORISSA',
            'PUNJAB',
            'PONDICHERRY',
            'RAJASTHAN',
            'SIKKIM',
            'TAMIL NADU',
            'TRIPURA',
            'UTTARAKHAND',
            'UTTAR PRADESH',
            'WEST BENGAL',
            'TELANGANA']
        state_obj = [State(state_name=x, country=country) for x in states]
        State.objects.bulk_create(state_obj)

        state = State.objects.get(state_name='KARNATAKA')

        districts = [
            'Bengaluru',
            'Tumkur',
            'Mysore']

        district_obj = [City(city_name=x, state=state) for x in districts]
        City.objects.bulk_create(district_obj)
        print("created country, states and cities...")
