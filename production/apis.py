"""
API definition

"""
from rest_framework import viewsets, serializers

from SERVO.pagination import Pagination
from production.models import Production


class ProductionSerializer(serializers.ModelSerializer):
    """
    ## Production Serializer:

    Serializer for Production model
    """
    class Meta:
        model = Production
        fields = ('id', 'bill_of_material')


class ProductionViewSet(viewsets.ModelViewSet):
    """
    ## Production ViewSet:

    viewset for ProductionSerializer model
    """
    queryset = Production.objects.all()
    serializer_class = ProductionSerializer
    pagination_class = Pagination
