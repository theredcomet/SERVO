import uuid

from django.db import models
from django.core import validators
from products.models import ProductMaster, Products


def get_bom_dir(instance, filename):
    "bom: bill of material"
    return '{}'.format(filename)


class ProductionTemplateModel(models.Model):
    """
    Configured Production Model for a given Final Product
        -   Defines Raw materials
        -   Defines Packing Materials
    """

    final_product = models.ForeignKey(Products, on_delete=models.CASCADE)
    remarks = models.TextField(null=True, blank=True)


class RawMaterialsTemplateModel(models.Model):
    """
    Raw materials details connected to Product (defined below)
    contains detail(s) of product categorised as *raw material* and number

    Closing stock is updated (clean method) before writing to database
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    production = models.ForeignKey(ProductionTemplateModel, on_delete=models.CASCADE)
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE,
        limit_choices_to={"ptype__id": "abd4dc63-0253-4b25-b340-4f4a9625cc00"})

    def __str__(self):
        return "{}".format(self.id)


class PackingMaterialsTemplateModel(models.Model):
    """
    Packing materials details connected to Product (defined below)
    contains detail(s) of product categorised as *packing material* and number

    Closing stock is updated (clean method) before writing to database
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    production = models.ForeignKey(ProductionTemplateModel, on_delete=models.CASCADE)
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE,
        limit_choices_to={"ptype__id": "a4e4a733-364b-4834-b782-5352cc57e86f"})

    def __str__(self):
        return "{}".format(self.id)


class Production(models.Model):
    """
    Conversion of raw material to Finished/Semi Finished products.

    -   Change Inventory number
        -   Substract (multiple) raw materials
        -   Add Inventory of Finished Products (one per row max)

    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    # raw_materials = models.ManyToManyField(RawMaterials)
    final_product = models.ForeignKey(Products, on_delete=models.CASCADE)

    # Date and Time details
    date = models.DateField()
    manufacture_date = models.DateField()
    expiry_date = models.DateField()
    mixing_time_from = models.TimeField()
    mixing_time_to = models.TimeField()
    packaging_time_from = models.TimeField()
    packaging_time_to = models.TimeField()
    no_man_power = models.PositiveIntegerField(default=1)

    # Finished Goods details
    opening_stock = models.PositiveIntegerField()
    product_yield = models.PositiveIntegerField(
        help_text="Quantity produced")
    closing_stock = models.PositiveIntegerField()

    bill_of_material = models.FileField(
        upload_to=get_bom_dir, null=True, blank=True)
    remarks = models.TextField(null=True, blank=True)

    def clean(self):
        # if self.bill_of_material:
        #     self.bill_of_material = None
        if not self.opening_stock:
            self.opening_stock = self.final_product.quantity
        if not self.closing_stock:
            self.closing_stock = self.final_product.quantity + self.opening_stock

    def __str__(self):
        return "{}".format(self.id)


class RawMaterials(models.Model):
    """
    Raw materials details connected to Product (defined below)
    contains detail(s) of product categorised as *raw material* and number

    Closing stock is updated (clean method) before writing to database
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    production = models.ForeignKey(Production, on_delete=models.CASCADE)
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE,
        limit_choices_to={"ptype__id": "abd4dc63-0253-4b25-b340-4f4a9625cc00"})
    quantity = models.PositiveIntegerField(default=1)
    closing_stock = models.PositiveIntegerField(blank=True)

    def clean(self):
        if not self.closing_stock:
            self.closing_stock = self.product.quantity + self.quantity

    def __str__(self):
        return "{}".format(self.id)


class PackingMaterials(models.Model):
    """
    Packing materials details connected to Product (defined below)
    contains detail(s) of product categorised as *packing material* and number

    Closing stock is updated (clean method) before writing to database
    """
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    production = models.ForeignKey(Production, on_delete=models.CASCADE)
    product = models.ForeignKey(
        Products,
        on_delete=models.CASCADE,
        limit_choices_to={"ptype__id": "a4e4a733-364b-4834-b782-5352cc57e86f"})
    quantity = models.PositiveIntegerField(default=1)
    closing_stock = models.PositiveIntegerField(blank=True)

    def clean(self):
        if not self.closing_stock:
            self.closing_stock = self.product.quantity + self.quantity

    def __str__(self):
        return "{}".format(self.id)
