from django.contrib import admin

from .models import Production, RawMaterials, PackingMaterials


class RawMaterialsAdmin(admin.TabularInline):
    model = RawMaterials
    list_display = [
        'id',
        'product',
        'quantity'
    ]
    fields = (
        'product',
        'quantity'
    )

class PackingMaterialsAdmin(admin.TabularInline):
    model = PackingMaterials
    list_display = [
        'id',
        'product',
        'quantity'
    ]
    fields = (
        'product',
        'quantity'
    )

class ProductionAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'date',
        'final_product',
        'product_yield'
    ]
    inlines = [RawMaterialsAdmin, PackingMaterialsAdmin]
    readonly_fields = ['bill_of_material',]
    fields = (
        'final_product',
        'date',
        ('manufacture_date', 'expiry_date'),
        ('mixing_time_from', 'mixing_time_to'),
        ('packaging_time_from', 'packaging_time_to'),
        'no_man_power',
        'product_yield',
        'remarks',
        'bill_of_material'
    )


admin.site.register(Production, ProductionAdmin)
# admin.site.register(RawMaterials, RawMaterialsAdmin)
