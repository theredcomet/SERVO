import os
import pdfkit
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string

from .models import Production


@receiver(post_save, sender=Production)
def create_bill_of_material(sender, **kwargs):
    print(kwargs)
    print(kwargs.get('signal'))
    print(kwargs.get('instance'))
    data = kwargs.get("instance")
    print(dir(data))
    template_name = "production/BillOfMaterial.html"
    pdf_file_name = "BillOfMaterial/{}.pdf".format(data.id)

    raw_mtrl = [x for x in data.rawmaterials_set.all()]
    pack_mtrl = [x for x in data.packingmaterials_set.all()]
    context = {
        "bom": data,
        "raw_materials": raw_mtrl,
        "packing_materials": pack_mtrl}
    
    html_str = render_to_string(template_name, context=context)

    if os.path.isfile("media/{}".format(pdf_file_name)):
        os.remove("media/{}".format(pdf_file_name))
    
    data.bill_of_material = pdf_file_name
    post_save.disconnect(sender=Production, receiver=create_bill_of_material)
    data.save()
    post_save.connect(sender=Production, receiver=create_bill_of_material)
    pdfkit.from_string(html_str, "media/{}".format(pdf_file_name))

