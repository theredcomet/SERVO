from django.apps import AppConfig


class ProductionConfig(AppConfig):
    name = 'production'
    def ready(self):
        from production import signals