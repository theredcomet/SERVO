from django.test import TestCase

from .models import Customer
from sales.funcs import get_test_data

class CustomerTestCase(TestCase):
    def setUp(self):
        self.data = get_test_data()
        Customer.objects.create(
            customer_name=self.data.get("customer_name"),
            phone_number=self.data.get("phone_number"))
    
    def test_created(self):
        customer = Customer.objects.filter(
            customer_name=self.data.get("customer_name")).last()
        self.assertEqual(self.data.get("customer_name"), customer.customer_name)
