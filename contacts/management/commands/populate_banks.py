import time
import csv
import os

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from contacts.models import Bank


class Command(BaseCommand):
    help = 'Adds data to models'

    def handle(self, *args, **options):
        Bank.objects.all().delete()
        
        banks = [
            'Allahabad Bank',
            'Andhra Bank',
            'Bank of Baroda',
            'Bank of India',
            'Bank of Maharashtra',
            'Canara Bank',
            'Central Bank of India',
            'Corporation Bank',
            'Dena Bank',
            'IDBI Bank',
            'Indian Bank',
            'Indian Overseas Bank',
            'Oriental Bank of Commerce',
            'Punjab and Sind Bank',
            'Punjab National Bank',
            'State Bank of India',
            'Syndicate Bank',
            'UCO Bank',
            'Union Bank of India',
            'Vijaya Bank',
            'Axis Bank',
            'Bandhan Bank',
            'Catholic Syrian Bank',
            'City Union Bank',
            'DCB Bank',
            'Dhanlaxmi Bank',
            'Federal Bank',
            'HDFC Bank',
            'ICICI Bank',
            'IDFC First Bank',
            'IndusInd Bank',
            'Jammu & Kashmir Bank',
            'Karnataka Bank',
            'Karur Vysya Bank',
            'Kotak Mahindra Bank',
            'Lakshmi Vilas Bank',
            'Nainital Bank',
            'RBL Bank',
            'South Indian Bank',
            'Tamilnad Mercantile Bank Limited',
            'Yes Bank']

        bank_objs = [Bank(name=bnk) for bnk in banks]
        Bank.objects.bulk_create(bank_objs)

        print("created banks...")
