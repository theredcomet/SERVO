from django.contrib import admin

from geodata.models import Locality
from .models import Company, Customer, Transport, Bank, BankAccount, Representative, Supplier


class TransportAdmin(admin.ModelAdmin):
    list_display = ['name', 'booking_place']


class BankAdmin(admin.ModelAdmin):
    list_display = ['name']
    search_fields = ['name']

class BankAccountAdmin(admin.ModelAdmin):
    list_display = ['account_no', 'account_name', 'bank', 'branch']


class RepresentativeAdmin(admin.ModelAdmin):
    list_display = ['representative_name',
                    'phone_number', 'email_id', 'commision', 'city', 'locality']


class SupplierAdmin(admin.ModelAdmin):
    list_display = ['supplier_name', 'phone_number',
                    'email_id', 'city', 'locality']


class LocalityAdmin(admin.ModelAdmin):
    pass


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['company_name', 'abbreviation']


class CustomerAdmin(admin.ModelAdmin):
    list_display = [
        'customer_name',
        'city',
        'locality']
    search_fields = [
        'id',
        'customer_name']
    list_filter = ['city', 'locality']


admin.site.register(Locality, LocalityAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Customer, CustomerAdmin)
admin.site.register(Transport, TransportAdmin)
admin.site.register(Bank, BankAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
admin.site.register(Representative, RepresentativeAdmin)
admin.site.register(Supplier, SupplierAdmin)
