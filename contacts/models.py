import uuid

from django.core.validators import validate_comma_separated_integer_list
from django.db import models

from geodata.models import City, Country, Locality, State


class Transport(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100, unique=True)
    booking_place = models.ForeignKey(Locality, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Bank(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.name


class BankAccount(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    account_no = models.BigIntegerField()
    ifsc = models.CharField(max_length=15, verbose_name="IFSC")
    account_name = models.CharField(
        max_length=50, verbose_name="Account Holder Name")
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    branch = models.ForeignKey(Locality, on_delete=models.CASCADE)

    def __str__(self):
        return self.account_name


class Representative(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    representative_name = models.CharField(max_length=50)

    phone_number = models.BigIntegerField()
    email_id = models.EmailField(null=True)

    commision = models.DecimalField(
        default=0.00, max_digits=4, decimal_places=2, verbose_name="Commission in percentage")

    address_1 = models.CharField(max_length=150, null=True, blank=True)
    address_2 = models.CharField(max_length=100, null=True, blank=True)
    state = models.ForeignKey(
        State, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, null=True, blank=True)
    locality = models.ForeignKey(
        Locality, on_delete=models.CASCADE, null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.representative_name


class Supplier(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    supplier_name = models.CharField(max_length=50)

    phone_number = models.BigIntegerField()
    email_id = models.EmailField(null=True)

    address_1 = models.CharField(max_length=150, null=True, blank=True)
    address_2 = models.CharField(max_length=100, null=True, blank=True)
    state = models.ForeignKey(
        State, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, null=True, blank=True)
    locality = models.ForeignKey(
        Locality, on_delete=models.CASCADE, null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.supplier_name


class Company(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    company_name = models.CharField(max_length=100, unique=True)
    company_display_name = models.CharField(
        max_length=100,
        help_text="This will be used in media (print, etc..)")
    abbreviation = models.CharField(
        max_length=7, help_text="Short hand for company name", null=True, blank=True)

    phone_number = models.BigIntegerField()
    address_1 = models.CharField(max_length=150)
    address_2 = models.CharField(max_length=100, null=True, blank=True)
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    city = models.ForeignKey(City, on_delete=models.CASCADE)
    locality = models.ForeignKey(Locality, on_delete=models.CASCADE)
    zipcode = models.IntegerField()

    gstin = models.CharField(max_length=20, verbose_name="GSTIN")
    description = models.CharField(max_length=250, null=True, blank=True)

    def __str__(self):
        return self.company_display_name


class Customer(models.Model):
    id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    customer_name = models.CharField(max_length=50)
    company_name = models.CharField(max_length=100, null=True)

    phone_number = models.BigIntegerField()
    email_id = models.EmailField(null=True)

    address_1 = models.CharField(max_length=150, null=True, blank=True)
    address_2 = models.CharField(max_length=100, null=True, blank=True)
    state = models.ForeignKey(
        State, on_delete=models.CASCADE, null=True, blank=True)
    city = models.ForeignKey(
        City, on_delete=models.CASCADE, null=True, blank=True)
    locality = models.ForeignKey(
        Locality, on_delete=models.CASCADE, null=True, blank=True)
    zipcode = models.IntegerField(null=True, blank=True)

    transport = models.ForeignKey(
        Transport, on_delete=models.CASCADE, null=True)

    bank_details = models.ForeignKey(BankAccount, on_delete=models.CASCADE, null=True)

    representative = models.ForeignKey(
        Representative, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.customer_name
